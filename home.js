// JavaScript Document
if (Meteor.isClient) {
	ERRORS_KEY = 'courseErrors';

		Template.home.onCreated(function() {
		  Session.set(ERRORS_KEY, {});
		  
		});
		
		Template.home.helpers({
		  errorMessages: function() {
			return _.values(Session.get(ERRORS_KEY));
		  },
		  errorClass: function(key) {
			return Session.get(ERRORS_KEY)[key] && 'error';
		  },
		  isLoggedin: function() {
			  	//Verify if user is logged in 
				//console.log(Meteor.userId())
				//console.log(JSON.stringify(Meteor.user()))
				if(Meteor.userId()==null) {
					Router.go('/signin');
					return false;
				}
				return true;
		  },
		  isTeacher: function() {
			  if(typeof Session.get('role')=='undefined') {
				  userrole = Roles.findOne({'userid':Meteor.userId()});
				  if(userrole)
					  Session.set('role', userrole.role);
			  }
			  if(Session.get('role')==='teacher')
			  	return true;
			  else
			  	return false;
		  },
		  teacherCourses: function() {
			  return Courses.find({'userid':Meteor.userId()});
		  },
		  studentCourses: function() {
			  var enrollments = Enrollment.find({'userid':Meteor.userId()});
			  c = []
			  enrollments.forEach(function(course){
				 c.push(course.courseid)
			   });
			  return Courses.find
		  },
		  specificFormData: function() {
		    return {
      		id: this._id,
			}
		  }
		});
		
		Template.home.events({
		  'click .create-course': function(event, template) {
			event.preventDefault();
			$(".done").trigger('click');
			$(".alert-danger", ".course-form").hide();
			$(".course-form").show();
			$(".course-form")[0].reset();
			template.$('[name=id]').val('');
			template.$('[name=file]').val('');
		  },
		  'click .glyphicon-pencil': function(event, template) {
			event.preventDefault();
			$(".course-form").show();
			$(".done").trigger('click');
			template.$('[name=id]').val(this._id);
			template.$('[name=title]').val(this.title);
		  },
		  'click .glyphicon-remove': function(event, template) {
			event.preventDefault();
			$(".course-form").hide();
			var confirmed = window.confirm("Do you really want to delete this course ?");
			if(confirmed){
				Courses.remove(this._id);
			}
		  },
		  'click .btn-cancel': function(event, template) {
			event.preventDefault();
			$(".course-form").hide();
		  },
		  'DOMSubtreeModified .progress-label': function(event, template) {
			  //event.preventDefault();
			  $(".start").trigger('click');
		  },
		  'submit': function(event, template) {
			event.preventDefault();
			
			var id = template.$('[name=id]').val();
			var title = template.$('[name=title]').val();
			var file = template.$('[name=file]').val();
			var coursesode = generateCourseCode(8);

			var errors = {};
		
			if (! title) {
			  errors.title = 'Title is required';
			}
		
			if (! coursesode) {
			  errors.coursesode = 'Course Code is required';
			}
			
			if($.trim(id)!=''){
				codeexists = Courses.findOne({'coursecode':coursesode, _id:{$ne:id}});
			} else {
				codeexists = Courses.findOne({'coursecode':coursesode});
				if (! file) {
				  errors.file = 'Attachment is required. Select the file and click on Upload';
				}
			}
			if(codeexists) {
				errors.coursesode = 'Course Code already exists';
			}
			
			Session.set(ERRORS_KEY, errors);
			if (_.keys(errors).length) {
			  return;
			}
			if($.trim(id)!=''){
				var data = { title: title};
				if (file) {
					data.attachmenturl = file;
				}
				Courses.update(id,{$set:data});
			} else {
				Courses.insert({
					userid: 	Meteor.userId(),
					title:  	title,
					coursecode: coursesode,
					attachmenturl: file,
					createdAt: 	new Date()
				});
			}
			$('.course-form').hide();
			//Router.go('home');
		  },
		  'click .find-form-btn': function(event, template) {
			event.preventDefault();
			$('.matching-courses').hide();
			var coursesode = template.$('[name=coursecode]').val();
			
			var errors = {};
		
			if (! coursesode) {
			  errors.coursesode = 'Course Code is required';
			}
			
			courseexists = Courses.find({'coursecode':coursesode});
			if(courseexists.length<=0) {
				errors.coursesode = 'Course does not exists';
			}
			
			Session.set(ERRORS_KEY, errors);
			if (_.keys(errors).length) {
			  return;
			}
			$('.matching-courses').find('tbody').html('');
			courseexists.forEach(function(course){
				  var rowstr = '<tr><td>'+course.title+'</td><td>'+course.coursecode+'</td>';
				  //if(course.attachmenturl)
				  //rowstr += '<a target="_blank" href="'+course.attachmenturl+'"><span title="Download" class="glyphicon glyphicon-floppy-save">&nbsp;</span></a>';
				  rowstr += '<td>&nbsp;';
				  rowstr += '<button type="submit" data-src="'+course._id+'" class="btn btn-success btn-join">Join</button>';
				  rowstr += '</td></tr>'
				$('.matching-courses').find('tbody').append(rowstr);
			});
			$('.matching-courses').show();
			return courseexists;
		  },
		  'click .btn-join': function(event, template) {
			event.preventDefault();
			var course_id = ($(event.target).data('src'));
			var alreadyjoined = Enrollment.findOne({userid: 	Meteor.userId(),courseid:  course_id});
			if(alreadyjoined) {
				alert('You have already joined that course')
				return;
			}
			Enrollment.insert({
					userid: 	Meteor.userId(),
					courseid:  course_id,
					createdAt: 	new Date()
				});
		  }
		});
	
}