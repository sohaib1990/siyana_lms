// JavaScript Document
Roles = new Mongo.Collection('roles');
Courses = new Mongo.Collection('courses');
Enrollment = new Mongo.Collection('enrollment'); 
if (Meteor.isServer) {
	Meteor.startup(function () {
	 //console.log(process.env);
	 var absolutePath = process.cwd()+'../../../../../../uploads';
	 console.log(absolutePath);
	  UploadServer.init({
		tmpDir: absolutePath+'/tmp',
		uploadDir: absolutePath,//process.env.PWD 
		checkCreateDirectories: true, //create the directories if doesn't exists
		getFileName: function(fileInfo, formData) { return Math.floor(Date.now() / 1000)+'-' + fileInfo.name; },
		mimeTypes: {
			"pdf": "application/pdf"
		},
		acceptFileTypes: /.pdf/i
	  });
	});
}
if (Meteor.isClient) {
	//Meteor.startup(function() {
	//  Uploader.uploadUrl = Meteor.absoluteUrl("upload"); // Cordova needs absolute URL
	//});
	Meteor.startup(function() {
	  Uploader.finished = function(index, fileInfo, templateContext) {
		//console.log(fileInfo.baseUrl+fileInfo.name);
		$('[name=file]').val(fileInfo.baseUrl+fileInfo.name);
	  }
	});
	generateCourseCode = function(len) {
		var s = Math.floor((Math.random() * 10) + 1).toString();
		while(s.length<len&&len>0){
			var r = Math.random();
			s+= (r<0.1?Math.floor(r*100):String.fromCharCode(Math.floor(r*26) + (r>0.5?97:65)));
		}
		return s;
	}
}