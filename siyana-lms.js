if (Meteor.isClient) {	
	//Routes
	Router.route('/signin', function () {
	  this.render('signin');
	});
	Router.route('/join', function () {
	  this.render('join');
	});
	Router.route('/home', function () {
	  this.render('home');
	  
	});

	Router.route('/course/:userText', function () {
	  this.render('course',{
	  	data:{
	  	SomeText: this.params.userText
	  }
	  	});
	});

	Router.route('/course', function () {
	  this.render('course');
	  
	});


	Router.route('/logout', function () {
	  Meteor.logoutOtherClients();
	  Meteor.logout();
	  Session.set('role','');
	  Router.go('/signin');
	});
	Router.route('/', function () {
	  this.render('signin'//, {
	  //data: function () { return Items.findOne({_id: this.params._id}); }}
	  );
	});
	
  // counter starts at 0
  Session.setDefault('counter', 0);
  
}//END isClient

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
